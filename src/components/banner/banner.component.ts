import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SharedDataService } from 'src/providers/shared-data/shared-data.service';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss'],
})
export class BannerComponent implements OnInit {
  //banner images
  slides = [
    "assets/images/banner_images/1.jpg",
    "assets/images/banner_images/2.jpg",
    "assets/images/banner_images/3.jpg"
  ];
  constructor(
    public shared: SharedDataService,
  ) { }

  ngOnInit() { }

}

import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  okText = "ok";
  alertText = "Alert";
  constructor(
    public alertCtrl: AlertController,
    public translate: TranslateService,
  ) {

  }
  async show(text) {
    this.translate.get([text, "ok", "Alert"]).subscribe( async (res) => {
      const alert = await this.alertCtrl.create({
        header: res["Alert"],
        message: res[text],
        buttons: [res["ok"]]
      });
      await alert.present();
    });
  }

  async showWithTitle(text, title) {
    this.translate.get([text, "ok", title]).subscribe(async (res) => {
      const alert = await this.alertCtrl.create({
        header: res[title],
        message: res[text],
        buttons: [res["ok"]]
      });
      await alert.present();
    });
  }
}

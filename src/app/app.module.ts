import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicStorageModule } from '@ionic/storage';

// Providers Import
import { ConfigService } from '../providers/config/config.service';
import { SharedDataService } from 'src/providers/shared-data/shared-data.service';
import { LocationDataService } from 'src/providers/location-data/location-data.service';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
// For Translation
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
// For Modals
import { PrivacyPolicyPage } from './privacy-policy/privacy-policy.page';
import { TermServicesPage } from './term-services/term-services.page';
import { RefundPolicyPage } from './refund-policy/refund-policy.page';
import { SelectCountryPage } from './select-country/select-country.page';
import { SelectZonesPage } from './select-zones/select-zones.page';
import { ScratchCardPage } from './scratch-card/scratch-card.page';
import { LoginPage } from './login/login.page';
import { SignUpPage } from './sign-up/sign-up.page';
import { ForgotPasswordPage } from './forgot-password/forgot-password.page';
import { LanguagePage } from './language/language.page';
import { CurrencyListPage } from './currency-list/currency-list.page';

import { FormsModule } from '@angular/forms';
//for side menu expandable
import { MenuComponentComponent } from '../components/menu-component/menu-component.component';
//for animation
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PrivacyPolicyPageModule } from './privacy-policy/privacy-policy.module';
import { TermServicesPageModule } from './term-services/term-services.module';
import { RefundPolicyPageModule } from './refund-policy/refund-policy.module';
import { SelectZonesPageModule } from './select-zones/select-zones.module';
import { SelectCountryPageModule } from './select-country/select-country.module';
import { SignUpPageModule } from './sign-up/sign-up.module';
import { LanguagePageModule } from './language/language.module';
import { ScratchCardPageModule } from './scratch-card/scratch-card.module';
import { LoginPageModule } from './login/login.module';
import { ForgotPasswordPageModule } from './forgot-password/forgot-password.module';
import { CurrencyListPageModule } from './currency-list/currency-list.module';
import { BlankModalPageModule } from './blank-modal/blank-modal.module';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { ThemeableBrowser, ThemeableBrowserOptions, ThemeableBrowserObject } from '@ionic-native/themeable-browser/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';


// For Translation
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent,
    MenuComponentComponent,
  ],
  entryComponents: [],
  imports: [
    BlankModalPageModule,
    PrivacyPolicyPageModule,
    TermServicesPageModule,
    RefundPolicyPageModule,
    SelectCountryPageModule,
    LoginPageModule,
    LanguagePageModule,
    ScratchCardPageModule,
    SignUpPageModule,
    CurrencyListPageModule,
    ForgotPasswordPageModule,
    SelectZonesPageModule,
    FormsModule,
    BrowserAnimationsModule,
    BrowserModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
  ],
  providers: [
    StatusBar,
    ConfigService,
    LocationDataService,
    SharedDataService,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    SpinnerDialog,
    OneSignal,
    ThemeableBrowser,
    AppVersion,
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
